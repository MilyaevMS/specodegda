window.BXDEBUG = true;
BX.ready(function(){
    var oPopup = new BX.PopupWindow('register_feedback', window.body, {
        autoHide : true,
        offsetTop : 0,
        offsetLeft : 0,
        lightShadow : true,
        closeIcon : true,
        closeByEsc : true,
        overlay: {
            backgroundColor: '#666666', opacity: '80'
        }
    });
    oPopup.setContent(BX('registration_form'));
    BX.bindDelegate(
        document.body, 'click', {className: 'button_registration_popup' },
        BX.proxy(function(e){
            if(!e)
                e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
        }, oPopup)
    );
});