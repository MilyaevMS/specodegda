<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>
<?
/***********************************************************************************
					form header
***********************************************************************************/
?>
	<span class="title-red">ЗАЯВКА</span> <span class="title-black"> НА РЕГИСТРАЦИЮ</span>
	<form>
	<?
	$countQuestions = 0;
/***********************************************************************************
						form questions
***********************************************************************************/
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
		{
			echo $arQuestion["HTML_CODE"];
		}
		else
		{
	?>
			<div class="question-field" <?=($countQuestions < 1) ? 'style="margin-top: 35px;"' : '';?>>
				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
					<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>

				<label><?=($arQuestion["CAPTION"] == "Email") ? 'Ваш <span style="color: #bc2209;">E-mail</span>' : $arQuestion["CAPTION"];?><?=($arQuestion["REQUIRED"] == "Y") ? $arResult["REQUIRED_SIGN"] : '';?></label>

				<?=$arQuestion["HTML_CODE"]?>
			</div>
	<?
		}
		$countQuestions++;
	}
	?>
		<input type="submit" value="" />
	</form>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>

<div class="right-bottom-block">
	<span>18 НОЯБРЯ 2016</span>
	<br />
	<span>MOSCOW CITY &#171;СЕВЕРНАЯ БАШНЯ&#187;</span>
</div>
