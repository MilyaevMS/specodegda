<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>
<div class="et_slide_in_menu_container" style="right: -320px; display: none;">
	<div class="et_pb_fullscreen_nav_container">
		<ul id="mobile_menu_slide" class="et_mobile_menu">
			<li id="menu-item-28714" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-28699 current_page_item menu-item-28714 et_first_mobile_item"><a href="/">Главная</a></li>
		</ul>
	</div>
</div>
<div id="et-main-area" style="margin-bottom: -24px;">
	<div id="main-content">
 <article id="post-28699" class="post-28699 page type-page status-publish hentry">
		<div class="entry-content">
			<div id="dotover" class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular">
				<div class=" et_pb_row et_pb_row_0">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_0">
						<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center et_pb_text_0">
							<h2>ЦЕРЕМОНИЯ НАГРАЖДЕНИЯ</h2>
						</div>
						<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_0 et_always_center_on_mobile et_pb_image_sticky et-animated">
 <img src="img/Logo-ptpp.png" alt="">
						</div>
						<div id="toptext" class="et_pb_promo et_pb_module et_pb_bg_layout_dark et_pb_text_align_center et_pb_cta_0 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2><span style="color: #a12713;"><span style="color: #f7f7f7;">ЗАБОТЛИВЫЙ РАБОТОДАТЕЛЬ </span>&nbsp;</span></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="next" class="et_pb_section et_pb_section_parallax et_pb_section_1 et_pb_with_background et_section_regular">
				<div class=" et_pb_row et_pb_row_1">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_1">
						<div class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_center et_pb_cta_1 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2>ЧТО ВАС <span style="color: #a12713;">ОЖИДАЕТ </span> НА ЦЕРЕМОНИИ</h2>
							</div>
						</div>
					</div>
				</div>
				<div class=" et_pb_row et_pb_row_2">
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_2">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_0 et_pb_blurb_position_left">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
 <img src="img/Награждение.png" class="et_pb_animation_top et-animated">
								</div>
								<div class="et_pb_blurb_container">
									<h4>НАГРАЖДЕНИЕ</h4>
								</div>
							</div>
						</div>
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_1 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_blurb_container">
									<p>
										 Проводим&nbsp; награждение лучших работодателей и партнеров
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_3">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_2 et_pb_blurb_position_left">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
 <img src="img/Демонстрация.png" class="et_pb_animation_top et-animated">
								</div>
								<div class="et_pb_blurb_container">
									<h4>ДЕМОНСТРАЦИЯ</h4>
								</div>
							</div>
						</div>
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_3 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_blurb_container">
									<p>
										 Демонстрируем передовые технологии изготовления спецодежды
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class=" et_pb_row et_pb_row_3">
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_4">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_4 et_pb_blurb_position_left">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
 <img src="img/Шоу%20программа.png" class="et_pb_animation_top et-animated">
								</div>
								<div class="et_pb_blurb_container">
									<h4>ШОУ ПРОГРАММА</h4>
								</div>
							</div>
						</div>
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_5 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_blurb_container">
									<p>
										 Прекрасно подготовленная презентация и показ модных трендов
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_5">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_6 et_pb_blurb_position_left">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
 <img src="img/Фуршет.png" class="et_pb_animation_top et-animated">
								</div>
								<div class="et_pb_blurb_container">
									<h4>ФУРШЕТ</h4>
								</div>
							</div>
						</div>
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_7 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_blurb_container">
									<p>
										 Великолепное меню, кофе брейк и ужин &nbsp;от известного шеф повара Парижа
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="dotover" class="et_pb_section et_pb_section_parallax et_pb_section_2 et_pb_with_background et_section_regular">
				<div class="et_parallax_bg et_pb_parallax_css" style="background-image: url('img/istock_000016806958_large.jpg');">
				</div>
				<div class=" et_pb_row et_pb_row_4 et_pb_equal_columns et_pb_gutters1 et_pb_row_fullwidth">
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_6">
						<div id="toptext" class="et_pb_promo et_pb_module et_pb_bg_layout_dark et_pb_text_align_center et_pb_cta_2 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2>КТО &nbsp;НАШИ<span style="color: #a12713;">&nbsp;ГОСТИ</span></h2>
								<p>
									 Представители каких структур являются гостями и участниками мероприятия
								</p>
							</div>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_7">
						<div class="wrong-mozila-block">
							<div id="toptext" class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_blurb_8 et_pb_blurb_position_top">
								<div class="et_pb_blurb_content">
									<div class="et_pb_blurb_container">
										<h4>БИЗНЕС</h4>
										<p>
											 Руководители бизнес и коммерческих структур различных сфер
										</p>
									</div>
								</div>
							</div>
							<div id="toptext" class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_blurb_9 et_pb_blurb_position_top">
								<div class="et_pb_blurb_content">
									<div class="et_pb_blurb_container">
										<h4>ОБЩЕСТВЕННОСТЬ</h4>
										<p>
											 Представители общественных объединений и различных комитетов
										</p>
									</div>
								</div>
							</div>
							<div id="toptext" class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_blurb_10 et_pb_blurb_position_top">
								<div class="et_pb_blurb_content">
									<div class="et_pb_blurb_container">
										<h4>ГОСУДАРСТВО</h4>
										<p>
											 Члены государственного совета по коммуникациям с бизнес сообществом
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
				<div class="et_pb_row et_pb_row_5 et_pb_gutters2 et_pb_row_4col">
					<div class="et_pb_column et_pb_column_1_4 et_pb_column_8">
						<div id="toptext" class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_cta_3 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2>ВАЖНОЕ</h2>
								<h2><span style="font-size: x-large; font-family: 'Open Sans';"><span style="color: #fcbf00;"><span style="color: #a02613;">БИЗНЕС СОБЫТИЕ</span></span></span></h2>
								<p>
									 Торгово промышленная палата г. Москвы и компания «СОЮЗСПЕЦОДЕЖДА » приглашают вас на незабываемое бизнес шоу
								</p>
							</div>
							<div class="button_registration_popup">
							</div>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_4 et_pb_column_9">
						<div id="rise" class="et_pb_module et-waypoint et_pb_image et_pb_animation_right et_pb_image_1 et_always_center_on_mobile">
 <a href="/zrabotodatel/img/item-0003.jpg" class="et_pb_lightbox_image" title=""> <img src="img/item-0003.jpg" alt=""> </a>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_4 et_pb_column_10">
						<div id="rise" class="et_pb_module et-waypoint et_pb_image et_pb_animation_top et_pb_image_2 et_always_center_on_mobile">
 <a href="/zrabotodatel/img/item-0001.jpg" class="et_pb_lightbox_image" title=""> <img src="img/item-0001.jpg" alt=""> </a>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_4 et_pb_column_11">
						<div id="rise" class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_3 et_always_center_on_mobile">
 <a href="/zrabotodatel/img/item-0004.jpg" class="et_pb_lightbox_image" title=""> <img src="img/item-0004.jpg" alt=""> </a>
						</div>
					</div>
				</div>
			</div>
			<div class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">
				<div class=" et_pb_row et_pb_row_6">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_12">
						<div class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_center et_pb_cta_4 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2>СПЕЦИАЛЬНЫЕ&nbsp;<span style="color: #a12713;">ГОСТИ&nbsp;</span>МЕРОПРИЯТИЯ</h2>
								<p>
									 VIP персоны и специальные гости нашей церемонии
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class=" et_pb_row et_pb_row_7">
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_13">
						<div class="et_pb_testimonial cards et_pb_testimonial_0 et_pb_icon_off et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_testimonial_no_bg clearfix">
							<div class="et_pb_testimonial_portrait" style="background-image: url('img/Вар.jpg');">
							</div>
							<div class="et_pb_testimonial_description">
								<div class="et_pb_testimonial_description_inner" style="width: 327px;">
									<p>
										 Варданян С. О.
									</p>
									<p>
										 Вице президент
									</p>
 <strong class="et_pb_testimonial_author">ТОРГОВО ПРОМЫШЛЕННАЯ ПАЛАТА</strong>
									<p class="et_pb_testimonial_meta">
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_1_2 et_pb_column_14">
						<div class="et_pb_testimonial card et_pb_testimonial_1 et_pb_icon_off et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_testimonial_no_bg clearfix" style="background-image: none;">
							<div class="et_pb_testimonial_portrait" style="background-image: url('img/шеф.jpg');">
							</div>
							<div class="et_pb_testimonial_description">
								<div class="et_pb_testimonial_description_inner" style="width: 327px;">
									<p>
										 Гамшеев&nbsp;&nbsp;Ю.В.
									</p>
									<p>
										 Генеральный&nbsp;директор
									</p>
 <strong class="et_pb_testimonial_author">КОМПАНИЯ СОЮЗСПЕЦОДЕЖДА</strong>
									<p class="et_pb_testimonial_meta">
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="et_pb_section et_pb_section_parallax et_pb_section_5 et_pb_with_background et_section_regular">
				<div class="et_parallax_bg et_pb_parallax_css" style="background-image: url('img/2016-09-18_15-13-31.jpg');">
				</div>
				<div class=" et_pb_row et_pb_row_8">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_15">
						<div class="et_pb_promo et_pb_module et_pb_bg_layout_dark et_pb_text_align_center et_pb_cta_5 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2>18 НОЯБРЯ&nbsp;&nbsp;<span style="color: #a12713;">2016</span></h2>
								<h1>MOSCOW CITY «СЕВЕРНАЯ БАШНЯ»</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="next1" class="et_pb_section et_pb_section_6 et_pb_with_background et_section_regular">
				<div class=" et_pb_row et_pb_row_9 et_pb_gutters2">
					<div class="et_pb_column et_pb_column_1_3 et_pb_column_16">
						<div class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_center et_pb_cta_6 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2><span style="color: #fcbf00;"><span style="color: #515151;">ПОЛЕЗНО&nbsp;И</span> <span style="color: #a12713;">ИНТЕРЕСНО</span></span></h2>
							</div>
						</div>
					</div>
					<div class="et_pb_column et_pb_column_2_3 et_pb_column_17">
						<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_text_1">
							<p>
 <span style="font-family: 'Open Sans';"><span style="font-size: 12pt; color: #222222;">На церемонии мы проводим&nbsp; награждение лучших&nbsp;работодателей страны и партнеров компании Союзспецодежда</span><span style="font-size: 12pt; color: #222222;"><br>
 </span></span>
							</p>
							<p>
 <span style="font-family: 'Open Sans';"><span style="font-size: 12pt;">У</span><span style="font-size: 12pt; line-height: 120%; color: #222222;">частники&nbsp;могут стать свидетелями шоу программы, &nbsp;демонстрирующей уникальные свойства&nbsp;спецодежды&nbsp;</span></span>
							</p>
							<p>
 <span style="font-family: 'Open Sans';"><span style="font-family: 'Open Sans';"><span style="font-size: 12pt; color: #222222;">Дружественная&nbsp;и непринужденная атмосфера, приветливый персонал и изысканный&nbsp; ужин от французского шеф повара </span><span style="font-size: 12pt; color: #a12713; font-weight: bold;">Жана </span><span style="font-size: 12pt; color: #a12713; font-weight: bold;">Люпена</span> <span style="font-size: 12pt; color: #a12713; font-weight: bold;">Депуа</span></span><span style="font-size: 12pt; color: #a12713; font-weight: bold;"><br>
 </span></span>
							</p>
						</div>
					</div>
				</div>
				<div class=" et_pb_row et_pb_row_10 et_pb_gutters1 et_pb_row_fullwidth">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_18">
						<div class="et_pb_module et_pb_gallery et_pb_gallery_0 et_pb_gallery_grid et_pb_bg_layout_light clearfix" style="display: block;">
							<div class="et_pb_gallery_items et_post_gallery" data-per_page="8">
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0011.jpg" title="item-0011"> <img alt="item-0011" src="img/item-0011-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0008.jpg" title="item-0008"> <img alt="item-0008" src="img/item-0008-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0010.jpg" title="item-0010"> <img alt="item-0010" src="img/item-0010-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0015.jpg" title="item-0015"> <img alt="item-0015" src="img/item-0015-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0009.jpg" title="item-0009"> <img alt="item-0009" src="img/item-0009-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0005-1.jpg" title="item-0005"> <img alt="item-0005" src="img/item-0005-1-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0013.jpg" title="item-0013"> <img alt="item-0013" src="img/item-0013-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0017.jpg" title="item-0017"> <img alt="item-0017" src="img/item-0017-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0018.jpg" title="item-0018"> <img alt="item-0018" src="img/item-0018-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0019.jpg" title="item-0019"> <img alt="item-0019" src="img/item-0019-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0020.jpg" title="item-0020"> <img alt="item-0020" src="img/item-0020-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0012.jpg" title="item-0012"> <img alt="item-0012" src="img/item-0012-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0014.jpg" title="item-0014"> <img alt="item-0014" src="img/item-0014-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0007-1.jpg" title="item-0007"> <img alt="item-0007" src="img/item-0007-1-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0016.jpg" title="item-0016"> <img alt="item-0016" src="img/item-0016-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
								<div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: none;">
									<div class="et_pb_gallery_image landscape">
 <a href="/zrabotodatel/img/item-0006-1.jpg" title="item-0006"> <img alt="item-0006" src="img/item-0006-1-400x284.jpg"> <span class="et_overlay"></span> </a>
									</div>
								</div>
							</div>
							<div class="et_pb_gallery_pagination">
								<ul>
									<li class="prev" style="display:none;"> <a href="#" data-page="prev" class="page-prev">Пред</a> </li>
									<li class="page page-1"> <a href="#" data-page="1" class="page-1 active">1</a> </li>
									<li class="page page-2"> <a href="#" data-page="2" class="page-2 last-page">2</a> </li>
									<li class="next"> <a href="#" data-page="next" class="page-next">Далее</a> </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class=" et_pb_row et_pb_row_11">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_19">
						<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left et_pb_text_2">
							<h1 style="line-height: 130%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: normal; text-align: center;"><span style="font-size: xx-large;"><span style="color: #a12713;">ЦЕРЕМОНИЯ&nbsp;НАГРАЖДЕНИЯ</span></span></h1>
							<p>
								 &nbsp;
							</p>
							<h2 style="line-height: 130%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: normal; text-align: center;"><span style="font-size: x-large;">ПОЗВОЛЯЕТ&nbsp;ВСЕМ УЧАСТНИКАМ ПОЛУЧИТЬ МАКИМАЛЬНУЮ ВЫГОДУ ОТ ПРОВЕДЕННОГО ВРЕМЕНИ И ОЩУТИТЬ&nbsp;МАССУ ПОЛОЖИТЕЛЬНЫХ ЭМОЦИЙ</span><span style="font-size: 11.0pt;"><br>
 </span></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="et_pb_section et_pb_section_7 et_pb_with_background et_section_regular">
				<div class=" et_pb_row et_pb_row_12">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_20">
						<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_4 et_always_center_on_mobile">
 <img src="img/a.png" alt="">
						</div>
						<div id="toptext" class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_center et_pb_cta_7 et_pb_no_bg">
							<div class="et_pb_promo_description">
								<h2>ЖАН ЛЮПЕН&nbsp;<span style="color: #a02411;">ДЕПУА</span></h2>
								<p>
									 &nbsp;
								</p>
								<p>
									 Весь вечер к вашим услугам изысканная кухня от шеф повара
								</p>
							</div>
							<div class="button_registration_popup" style="margin: 0 auto">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="et_pb_section et_pb_section_8 et_pb_with_background et_section_specialty">
				<div class="et_pb_row">
					<div class="et_pb_column et_pb_column_1_3 et_pb_column_21 et_pb_column_single">
						<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_text_3">
							<blockquote>
								<p>
 <span style="font-family: 'Open Sans';">МЫ В СОЦ СЕТЯХ</span>
								</p>
							</blockquote>
						</div>
						<ul id="toptext" class="et_pb_social_media_follow et_pb_module et_pb_bg_layout_dark et_pb_social_media_follow_0 clearfix">
							<li class="et_pb_social_icon et_pb_social_network_link et-social-vk et_pb_social_media_follow_network_0"> <a href="https://vk.com/souzspecodegda" class="icon circle" title="Присоединяйтесь к нам в Вконтакте" style="background-image: url('img/vk32n.png');"></a> </li>
							<li class="et_pb_social_icon et_pb_social_network_link et-social-facebook et_pb_social_media_follow_network_0"> <a href="https://www.facebook.com/souzspecodegda/" class="icon circle" title="Присоединяйтесь к нам в Facebook" style="background-color: #3b5998;"></a> </li>
							<li class="et_pb_social_icon et_pb_social_network_link et-social-twitter et_pb_social_media_follow_network_1"> <a href="https://twitter.com/specodegda_ru" class="icon circle" title="Присоединяйтесь к нам в Twitter" style="background-color: #00aced;"></a> </li>
						</ul>
					</div>
					<div class="et_pb_column et_pb_column_2_3 et_pb_column_22 et_pb_specialty_column">
						<div class=" et_pb_row_inner et_pb_row_inner_0">
							<div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_0">
								<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_text_4">
									<blockquote>
										<p>
											 ЦЕРЕМОНИЯ НАГРАЖДЕНИЯ ЗАБОТЛИВЫЙ РАБОТОДАТЕЛЬ
										</p>
									</blockquote>
								</div>
							</div>
						</div>
						<div class=" et_pb_row_inner et_pb_row_inner_1">
							<div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_1">
								<div class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_blurb_11 et_pb_blurb_position_left">
									<div class="et_pb_blurb_content">
										<div class="et_pb_main_blurb_image">
 <span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #a12713;"></span>
										</div>
										<div class="et_pb_blurb_container">
											<p>
 <span style="font-size: 12pt; font-family: 'Open Sans';">Москва. М.&nbsp;Международная</span>
											</p>
											<p style="line-height: 120%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: normal;">
 <span style="font-family: 'Open Sans';"><span style="font-size: 12pt;">Ул. </span><span style="font-size: 12pt;">Текстовская</span><span style="font-size: 12pt;"> дом 10. Северная башня.&nbsp; Вход 1</span><span style="font-size: 12pt; color: #222222;"><br>
 </span></span>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_2">
								<div class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_blurb_12 et_pb_blurb_position_left">
									<div class="et_pb_blurb_content">
										<div class="et_pb_main_blurb_image">
 <span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #a12713;"></span>
										</div>
										<div class="et_pb_blurb_container">
											<p>
 <a style="font-size: 12pt; line-height: 120%; font-family: 'Open Sans'; color: #ffffff; cursor: text;" href="mailto:ceremonia@specodegda.ru">ceremonia@specodegda.ru</a>
											</p>
										</div>
									</div>
								</div>
								<div class="et_pb_blurb et_pb_module et_pb_bg_layout_dark et_pb_text_align_left et_pb_blurb_13 et_pb_blurb_position_left">
									<div class="et_pb_blurb_content">
										<div class="et_pb_main_blurb_image">
 <span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #a12713;"></span>
										</div>
										<div class="et_pb_blurb_container">
											<p style="line-height: 120%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: normal;">
 <span style="font-size: 12pt; font-family: 'Open Sans';">+7 (916) 607 04 22</span>
											</p>
											<p style="line-height: 120%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: normal;">
 <span style="font-size: 12.0pt; font-family: 'Source Sans Pro'; mso-ascii-font-family: 'Source Sans Pro'; mso-fareast-font-family: 'Source Sans Pro'; mso-bidi-font-family: 'Source Sans Pro'; color: #222222; mso-color-index: 1; mso-font-kerning: 12.0pt; language: en-US;">&nbsp;</span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
 </article>
	</div>
</div>
 <br>