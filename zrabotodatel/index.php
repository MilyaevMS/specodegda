<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Заботливый работодатель");
$APPLICATION->SetPageProperty("title", "Заботливый работодатель");
$APPLICATION->SetPageProperty("description", "Заботливый работодатель");

define("WEB_FORM_REGISTRATION_ID", 15);
?>
<!DOCTYPE html>
<html class="js" lang="ru-RU">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title><?$APPLICATION->ShowTitle();?></title>
	<?$APPLICATION->ShowHead();?>
</head>
<body class="home page page-id-28699 page-template-default et_pb_button_helper_class et_fixed_nav et_hide_nav et_cover_background et_pb_gutter linux et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_slide et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme gecko">
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div id="page-container" style="padding-top: 0;" class="et-animated-content">
		<?
		$APPLICATION->IncludeFile(
			SITE_DIR."zrabotodatel/content.php",
			Array(),
			Array("MODE"=>"html")
		);
		?>
		<div id="registration_form" style="display:none;">
			<?$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"register_form",
				Array(
					"WEB_FORM_ID" => WEB_FORM_REGISTRATION_ID,
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "N",
					"SEF_MODE" => "N",
					"CACHE_TYPE" => "N",
					"CACHE_TIME" => "3600",
					"LIST_URL" => "",
					"EDIT_URL" => "",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"VARIABLE_ALIASES" => Array(
						"WEB_FORM_ID" => "WEB_FORM_ID",
						"RESULT_ID" => "RESULT_ID"
					),
					"AJAX_MODE" => "Y",  // режим AJAX
					"AJAX_OPTION_SHADOW" => "N", // затемнять область
					"AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента
					"AJAX_OPTION_STYLE" => "Y", // подключать стили
					"AJAX_OPTION_HISTORY" => "N"
				)
			);?>
		</div>
	</div>
<?
CJSCore::Init(array("jquery", "popup"));
$arSite = CSite::GetByID("s1")->Fetch();
$page = $arSite["SERVER_NAME"] . $APPLICATION->GetCurPage();

$APPLICATION->AddHeadString("<link rel='canonical' href='http://{$page}' />");
$APPLICATION->AddHeadString("<link rel='shortlink' href='http://{$page}' />");

$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/css_002.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/css_005.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/style.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/shortcodes.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/shortcodes_responsive.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/magnific_popup.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/replaces.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/css_004.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/css_003.css");
$APPLICATION->SetAdditionalCSS("/zrabotodatel/css/css.css");

$APPLICATION->AddHeadScript("/zrabotodatel/js/frontend-builder-global-functions.js");
$APPLICATION->AddHeadScript("/zrabotodatel/js/waypoints.js");
$APPLICATION->AddHeadScript("/zrabotodatel/js/jquery_005.js");
$APPLICATION->AddHeadScript("/zrabotodatel/js/frontend-builder-scripts.js");
?>
</body>
</html>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
